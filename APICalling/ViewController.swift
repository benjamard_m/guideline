import UIKit
import Foundation
import Alamofire

class ViewController: UIViewController {
  @IBOutlet weak var getButton: UIButton!
  @IBOutlet weak var postButton: UIButton!
  @IBOutlet weak var putButton: UIButton!
  @IBOutlet weak var patchButton: UIButton!
  @IBOutlet weak var deleteButton: UIButton!
  @IBOutlet weak var standardButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
  }
  

  @IBAction func onClickCallWithStandard(_ sender: Any) {
    // เรียก API ด้วยวิธีปกติที่ใช้ Library ของ Swift เอง
    // 1. กำหนด url
    let getUrlString = "https://jsonplaceholder.typicode.com/todos/1"
    let url = URL(string: getUrlString)!
    // 2. สร้าง request
    var request = URLRequest(url: url)
    // 3. กำหนด Http Request Method
    request.httpMethod = "GET"
    
    // 4. สร้าง session และ Call api เลย !
    URLSession.shared.dataTask(with: request, completionHandler: { data, response, error -> Void in
    // 5. ถ้ามี error ก็ ทำอะไรกับมันสักอย่าง
      if error != nil {
        print(error ?? "")
        return
      } else {
    // 6. ถ้า success ก็นำ response ที่ได้ map เข้า model
        self.mappingDataToModelWithGetMethod(data: data!)
      }
      
    }).resume()
    
  }
  
  @IBAction func onClickGetMethod(_ sender: Any) {
    // เรียก API (GET Method) ด้วย Alamofire Library
    // 1. กำหนด url
    let getUrlString: String = "https://jsonplaceholder.typicode.com/todos/1"
    // 2. สร้าง request แล้วยิงเลย !
    Alamofire.request(getUrlString)
      .responseJSON { response in
    // 3. ถ้ามี error ก็ทำอะไรกับมันสักอย่าง
        if let error = response.result.error {
          print(error)
        }
    // 4. ถ้า success แล้วก็ นำไป map เข้า model เพื่อนำไปใช้งานต่อ
        if let value = response.data {
          self.mappingDataToModelWithGetMethod(data: value)
        }
      }
  }
  
  @IBAction func onClickPostMethod(_ sender: Any) {
    // 1. เรียก API (POST Method) ด้วย Alamofire Library
    // 1. กำหนด url
    let postUrlString = "https://jsonplaceholder.typicode.com/posts"
    // 2. สร้าง body param ที่จะส่งไปให้ API
    let parameters: [String: Any] = [
      "title": "foo",
      "body": "bar",
      "userId": 1
    ]
  
    // 3. สร้าง request แล้วยิงเลย !
    Alamofire.request(postUrlString, method: .post, parameters: parameters, encoding: JSONEncoding.default)
      .responseJSON { response in
        // 3. ถ้ามี error ก็ทำอะไรกับมันสักอย่าง
        if let error = response.result.error {
          print(error)
        }
        // ถ้า success นำไปใช้งานต่อ
        if let value = response.result.value {
          print(value)
        }
    }
  }
  
  @IBAction func onClickPutMethod(_ sender: Any) {
    // 1. เรียก API (PUT Method) ด้วย Alamofire Library
    // 1. กำหนด url
    let putUrlString = "https://jsonplaceholder.typicode.com/posts"
    // 2. สร้าง body param ที่จะส่งไปแก้ไขค่าที่ API
    let parameters: [String: Any] = [
      "id": 1,
      "title": "title",
      "body": "put",
      "userId": 1
    ]
    
    // 3. สร้าง request แล้วยิงเลย !
    Alamofire.request(putUrlString, method: .put, parameters: parameters, encoding: JSONEncoding.default)
      .responseJSON { response in
        // 3. ถ้ามี error ก็ทำอะไรกับมันสักอย่าง
        if let error = response.result.error {
          print(error)
        }
        // ถ้า success
        print(response.result)
    }
  }
  
  @IBAction func onClickPatchMethod(_ sender: Any) {
    // 1. เรียก API (Patch Method) ด้วย Alamofire Library
    // 1. กำหนด url
    let patchUrlString = "https://jsonplaceholder.typicode.com/posts"
    // 2. สร้าง body param ที่จะส่งไปแก้ไขค่าที่ API
    let parameters: [String: Any] = [
      "title": "patchTitle",
    ]
    
    // 3. สร้าง request แล้วยิงเลย !
    Alamofire.request(patchUrlString, method: .patch, parameters: parameters, encoding: JSONEncoding.default)
      .responseJSON { response in
        // 3. ถ้ามี error ก็ทำอะไรกับมันสักอย่าง
        if let error = response.result.error {
          print(error)
        } else{
          // ถ้า success
          print(response.result)
          
        }
       
    }
  }
  
  @IBAction func onClickDeleteMethod(_ sender: Any) {
    // เรียก API (DELETE Method) ด้วย Alamofire Library
    // 1. กำหนด url (ในตัวอย่างจะส่ง id ของ post ไปพร้อมกับ url)
    let deleteUrlString: String = "https://jsonplaceholder.typicode.com/posts/1"
    // 2. สร้าง request แล้วยิงเลย !
    Alamofire.request(deleteUrlString, method: .delete, encoding: JSONEncoding.default)
      .responseJSON { response in
        // 3. ถ้ามี error ก็ทำอะไรกับมันสักอย่าง
        if let error = response.result.error {
          print(error)
        } else {
          // ถ้า success
          print(response.result)
        }
    }
  }
  
  
  func mappingDataToModelWithGetMethod(data: Data) {
    do {
      let jsonDecoder = JSONDecoder()
      let responseModel = try jsonDecoder.decode(DataModel.self, from: data)
      print(responseModel)
    } catch {
      print("JSON Serialization error")
    }
  }
  
}

