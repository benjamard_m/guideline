import Foundation
import UIKit

struct DataModel: Codable {
  var userId: Int
  var id: Int
  var title: String
  var completed: Bool
}


struct ReqPostModel: Codable {
  var title: String
  var userId: Int
  var body: String
  
  init(title: String, userId: Int, body: String) {
    self.title = title
    self.userId = userId
    self.body = body
  }
}
